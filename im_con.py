from pox.core           import core
from pox.lib.revent     import *
from pox.lib.util       import dpidToStr
from pox.lib.addresses  import IPAddr, EthAddr

import pox.openflow.libopenflow_01 as of
import json
import networkx as nx

log = core.getLogger()

HARD_TIMEOUT = 30
IDLE_TIMEOUT = 30

class LearningSwitch (EventMixin):

    def __init__ (self, connection, parent, name):
        # Initiates a switch to which we'll be adding L2 learning capabilities
        self.macToPort = {}
        self.dpid = connection.dpid
        self.connection= connection
        self.listenTo(connection)
        self.parent = parent
        self.ports = [port.port_no for port in self.connection.features.ports[1:]]
        self.netMapArp = {}
        self.name = name

    def _handle_PacketIn (self, event):

        # parsing the input packet
        packet = event.parse()

        # updating out mac to port mapping
        self.macToPort[packet.src] = event.port

        if packet.type == packet.LLDP_TYPE or packet.type == 0x86DD:
            # Drop LLDP packets
            # Drop IPv6 packets
            # Send of command without actions

            log.debug("Port type is LLDP or IPv6 -- dropping")
            msg    = of.ofp_packet_out()
            msg.buffer_id = event.ofp.buffer_id
            msg.in_port = event.port
            self.connection.send(msg)

            return
        if packet.find("arp"):
            arpPacket = packet.find("arp")
            src = str(arpPacket.protosrc)
            dst = str(arpPacket.protodst)
            if src not in self.parent.ip2host or dst not in self.parent.ip2host:
                print "Big error", src, dst
                return
            srcHost = self.parent.ip2host[src]
            dstHost = self.parent.ip2host[dst]

            queueId = 0
            if (src,dst) in self.parent.flows:
                queueId = self.parent.flows[(src, dst)]
            elif (dst, src) in self.parent.flows:
                queueId = self.parent.flows[(dst, src)]

            path = self.parent.paths.get((srcHost, dstHost), None)
            if path is None:
                path = nx.shortest_path(self.parent.graph, srcHost, dstHost)
                self.parent.addPath((srcHost, dstHost), path, queueId)
                # self.parent.paths[(srcHost, dstHost)] = path
            # print path
            for i in xrange(len(path)):
                if path[i] == self.name:
                    break

            nextHop = path[i+1]
            nextHopPort = self.parent.hops[self.name]["hop"][nextHop]
            msg = of.ofp_packet_out()
            msg.buffer_id = event.ofp.buffer_id
            msg.actions.append(of.ofp_action_output(port = nextHopPort))
            self.connection.send(msg)
            return

        elif packet.find("ipv4"):
            ipPkt = packet.find("ipv4")
            # import pdb; pdb.set_trace()
            src = str(ipPkt.srcip)
            dst = str(ipPkt.dstip)
            if src not in self.parent.ip2host or dst not in self.parent.ip2host:
                print "Big error", src, dst
                return
            srcHost = self.parent.ip2host[src]
            dstHost = self.parent.ip2host[dst]
            path = self.parent.paths.get((srcHost, dstHost), None)
            if path is None:
                print "New Path"
                path = nx.shortest_path(self.parent.graph, srcHost, dstHost)
                self.parent.paths[(srcHost, dstHost)] = path
            # print path
            for i in xrange(len(path)):
                if path[i] == self.name:
                    break

            queueId = 0
            if (src,dst) in self.parent.flows:
                queueId = self.parent.flows[(src, dst)]
            elif (dst, src) in self.parent.flows:
                queueId = self.parent.flows[(dst, src)]

            print self.name, srcHost, dstHost, str(src), str(dst), queueId, "Ip packet dude :)"

            nextHop = path[i+1]
            nextHopPort = self.parent.hops[self.name]["hop"][nextHop]

            msg = of.ofp_flow_mod()
            msg.match = of.ofp_match.from_packet(packet, event.port)
            if self.name == path[1]:
                msg.actions.append(of.ofp_action_enqueue(port = nextHopPort, queue_id = queueId))
            else:
                msg.actions.append(of.ofp_action_output(port = nextHopPort))
            msg.data = event.ofp
            self.connection.send(msg)
            return

class learning_switch (EventMixin):

    _neededComponents = set(['openflow_discovery'])
    graph = None
    hops = None
    ip2host = None
    flows = None

    paths = {} #key->(src,dst), value node Name

    def __init__(self, hops, graph, ip2host, flows):
        super(EventMixin, self).__init__()
        if not core.listen_to_dependencies(self, self._neededComponents):
            self.listenTo(core)
        self.flows = flows
        self.listenTo(core.openflow)
        self.graph = graph
        self.hops = hops
        self.ip2host = ip2host

    def _handle_ConnectionUp (self, event):
        name = event.connection.features.ports[0].name
        log.debug("Connection %s" % (event.connection,))
        LearningSwitch(event.connection, self, name)

    def addPath(self, key, value, queueId):
        q2weight = {0:100, 1:4, 2:6}
        self.paths[key] = value
        for i in xrange(1, len(value)):
            self.graph.remove_edge(value[i-1], value[i])
            self.graph.add_edge(value[i-1], value[i], weight=q2weight.get(queueId, 1))

def createTopologyFromData(data):
    switches = set(data["switches"])
    # print switches
    G = nx.Graph()
    interfaces = {}
    ip2host = {data["hosts"][x]:x for x in data["hosts"]}
    for lnk in data["links"]:
        x, y, z = lnk
        G.add_edge(x, y, weight=z)
        p = [[x,y], [y,x]]
        for m in p:
            if m[0] in switches:
                lst = interfaces.setdefault(m[0], {"port":{}, "hop":{}})
                ln = len(lst["port"]) + 1
                lst["port"][ln] = m[1]
                lst["hop"][m[1]] = ln
    return interfaces, G, ip2host

def processFlows(flows):
    output = {}
    for flow in flows:
        key = (flow[0], flow[1])
        value = flow[2] + 1
        output[key] = value
    return output

def launch (conf_file, flowfile):
    fp = open(conf_file)
    data = json.load(fp)
    fp.close()

    # import pdb; pdb.set_trace()
    fp = open(flowfile)
    flows = json.load(fp)
    fp.close()
    flows = processFlows(flows)

    hops, G, ip2host = createTopologyFromData(data)
    # print json.dumps([hops, ip2host])
    core.registerNew(learning_switch, hops, G, ip2host, flows)
