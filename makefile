# Your linux usename
USERNAME = xavier666

# Repository location
REPO_LOC = Documents/git_stuff/traffic_emulation

# . : IMPORTANT : .
# Modify the variables according to where you put the repo in your local machine

# location of the POX executable
POX_PATH  = /home/$(USERNAME)/Documents/git_stuff/pox/pox.py

# location of the topology.json file
TOPO_PATH = /home/$(USERNAME)/$(REPO_LOC)/topology.json

# location of the flows.json file
FLOW_PATH = /home/$(USERNAME)/$(REPO_LOC)/flows.json

# location of where to put the controller file
CON_PATH  = /home/$(USERNAME)/Documents/git_stuff/pox/pox/forwarding/im_con.py

# running the controller
topo:
	@sudo python im_topo.py topology.json flows.json

# deleting extrneous files
clean:
	@ - sudo rm *.mp4 *.sdp
	@sudo mn -c

# running the controller
con:
	@cp im_con.py $(CON_PATH)
	@$(POX_PATH) log.level --DEBUG forwarding.im_con --conf_file=$(TOPO_PATH) --flowfile=$(FLOW_PATH)
