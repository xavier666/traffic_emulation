# Traffic Generation In A Large Topology
We are trying to measure how traffic flows are affected in a multihop network.
We are using `pox` as the SDN controller and `mininet` as the network emulator.  

## Prerequisites
 * Python modules required are `networkx`, `pox`, `mininet`
 * A video file upon which QoS is measured

## Note
 * `im_con.py`     - Contains the controller logic
 * `im_topo.py`    - Creates the multihop topology
 * `flow.json`     - Contains the list of flows to be emulated
 * `topology.json` - Contains the multihop topology

## To-do before running the code
 * Say you have downloaded the repo at location `LOC=/home/user/traffic_emulation`
 * Modify all the paths in the makefiles
  * Location of your installed POX directory
  * Location of topology.json file = `LOC/topology.json`
  * Location of flows.json file = `LOC/flows.json`
  * Location of where to put the controller file. You can put it in `pox/pox/forwarding/`
 * Modify the location of the video file in `im_config.py`
 
## To Run The Code
 * Clean previous virtual interfaces, sdp files and mp4 files by `make clean`
 * Start the controller by `make con`
 * Start the topology + emulation by `make topo`
