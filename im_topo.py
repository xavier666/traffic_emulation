from mininet.topo import Topo
from mininet.net  import Mininet
from mininet.cli  import CLI
from mininet.link import TCLink
from mininet.node import RemoteController
from functools    import partial

import time
import random
import sys, json
import subprocess as sp
import im_config

# Global variables

# Input file name
inpFl               = im_config.path_video_file()
# Little more than the length of the video file
local_stream_time   = im_config.local_stream_time()                 

class MyTopo(Topo):
	# not called
    def createGrid(self, rows = 3, cells = 3):
        """Creates a grid of switches 3x6"""

        switches = []
        cnt = -1
        for row in xrange(rows):
            for cell in xrange(cells):
                s = self.addSwitch("s%02d%02d"%(row+1, cell+1))
                cnt += 1
                if row > 0:
                    self.addLink(switches[cnt - cells], s)
                if cell > 0:
                    self.addLink(switches[-1], s)
                switches += [s]
        return switches

    def createTopologyFromData(self, data):
        """Creates links in between the switches from a given topology file"""

        switches = {}
        hosts = {}
        hops = {}

        for swt in data["switches"]:
            st = self.addSwitch(swt)
            switches[swt] = st
            hops[swt] = st

        for hst in data["hosts"]:
            ht = self.addHost(hst, ip=data["hosts"][hst])
            hosts[hst] = ht
            hops[hst] = ht

        for lnk in data["links"]:
            ln = self.addLink(hops[lnk[0]], hops[lnk[1]])

        return hops

    def build(self, _max_bw, data):
        return self.createTopologyFromData(data)

def create_sdp_file(dstIP, sdp_file_name):

	file_contents = 'SDP:\nv=0\no=- 0 0 IN IP4 127.0.0.1\ns=No Name\nt=0 0\na=tool:libavformat 56.40.101\nm=video 6005 RTP/AVP 96\nc=IN IP4 %s\nb=AS:2055\na=rtpmap:96 H264/90000\na=fmtp:96 packetization-mode=1; sprop-parameter-sets=Z01AKOygUBf8uAtQEBAUAAADAAQACvyAPGDGWA==,aO+G8g==; profile-level-id=4D4028\nm=audio 7005 RTP/AVP 97\nc=IN IP4 %s\nb=AS:159\na=rtpmap:97 MPEG4-GENERIC/48000/2\na=fmtp:97 profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3; config=1190\n' % (dstIP, dstIP)

	fp = open(sdp_file_name, 'w')

	fp.write(file_contents)

	fp.close()

def stream_string(option, dstIP, filename, local_stream_time):
    """Creates the string which will be executed on client/server side"""

    command             = None
    sdp_file_name       = 'sdp_to_' + dstIP + '.sdp'

    # Server mode
    if option == 1:
        # ffmpeg -re -thread_queue_size 4 -i ~/Videos/source.mp4 -strict -2 -vcodec copy -an -f rtp rtp://10.0.0.36:6005 -acodec copy -vn -sdp_file random_sdp_file -f rtp rtp://10.0.0.36:7005
        # command = 'ffmpeg -re -thread_queue_size 4 -i %s -strict -2 -vcodec copy -an -f rtp rtp://%s:6005 -acodec copy -vn -sdp_file %s -f rtp rtp://%s:7005 &' % (filename, dstIP, sdp_file_name, dstIP)

        command = 'ffmpeg -re -thread_queue_size 4 -i %s -strict -2 -vcodec copy -an -f rtp rtp://%s:6005 -acodec copy -vn -f rtp rtp://%s:7005 &' % (filename, dstIP, dstIP)

    # Client mode
    else:
        create_sdp_file(dstIP, sdp_file_name)

        # ffmpeg -i random_sdp_file -strict -2 -timelimit 120 random_video_file.mp4
        # command = 'ffmpeg -i %s -strict -2 -timelimit %s %s &' % (sdp_file_name, local_stream_time, filename)

        command = 'ffmpeg -i %s -strict -2 %s &' % (sdp_file_name, filename)

    return command

def stream(src, dst, input_filename, output_filename, dstIP):
    """Streams video from src to dst"""

    global local_stream_time

    if src.waiting or dst.waiting:
        print src, dst, "stream: something went wrong"
        return
    else:
        print src, dst, "stream: working"

    # For the next line, see `stream.py` for a better understanding of the arguments

    client_command = stream_string(0, dstIP, output_filename, local_stream_time)
    server_command = stream_string(1, dstIP, input_filename, local_stream_time)

    print ' > CLI_CMD :', client_command
    print ' > SRV_CMD :', server_command

    dst.cmd(client_command) # Waits for a stream
    print 'Client command executed'

    time.sleep(1)

    src.cmd(server_command) # Begins sending the stream
    print 'Server command executed'

    return (src, dst)

def file_transfer(src, dst, inputDirname, filename, srcIP):
    """Creates short flows between a given source and destination"""

    if src.waiting or dst.waiting:
        print src, dst, "http: something went wrong"
        return
    else:
        print src, dst, "http: working"

    serverComd = "webfsd -F -p 8000 -r "+inputDirname
    src.sendCmd(serverComd)
    clientComd = "wget http://%s:8000/%s"%(srcIP, filename)
    dst.sendCmd(clientComd)

    return src, dst

def processFlows(flows, data):
    """Creates flows from a given flow file"""

    ip2Host = {}
    for dt in data["hosts"]:
        ip2Host[data["hosts"][dt]] = dt
    flowsOut = []
    for flow in flows:
        src = ip2Host[flow[0]]
        dst = ip2Host[flow[1]]

        flowsOut += [[src, dst]+flow[2:]]
    return flowsOut

def startFlows(flows, net):
    """Initiates the flows"""

    global inpFl

    for flow in flows:
        # time.sleep(1)
        dst = net.get(flow[1])
        src = net.get(flow[0])

        if flow[2] == 1: #realtime i.e. vlc
            opFl = 'video_' + src.IP().strip() + '_' + dst.IP().strip() + '.mp4'
            dstIP = dst.IP().strip()
            stream(src, dst, inpFl, opFl, dstIP)

        else:
            srcIP = src.params["ip"]
            # file_transfer(src, dst, "/home/abhijit/Downloads", "1", srcIP)

def main():
    # loading topology data
    fp = open(sys.argv[1])
    data = json.load(fp)
    fp.close()

    # loading flow data
    fp = open(sys.argv[2])
    flows = json.load(fp)
    fp.close()

    flows = processFlows(flows, data)

    # All b/w are in megabits
    max_bw = 1000

    # B/w of queue 1 and queue 2
    values = [100,100]

    topo = MyTopo(max_bw, data)

    net = Mininet(topo, link = TCLink, controller = partial(RemoteController, ip = '127.0.0.1', port = 6633))

    net.start()

    # Creating queue command
    cmd = 'sudo ovs-vsctl -- set port %%s qos=@newqos -- \
        --id=@newqos create QoS type=linux-htb other-config:max-rate=1000000000 queues=0=@q0,1=@q1,2=@q2 -- \
        --id=@q0 create Queue other-config:min-rate=%d other-config:max-rate=%d -- \
        --id=@q1 create Queue other-config:min-rate=%d other-config:max-rate=%d -- \
        --id=@q2 create Queue other-config:min-rate=%d other-config:max-rate=%d' % \
        (max_bw * 10**6, max_bw * 10**6, \
        0, values[0] * 10**6, \
        0, values[1] * 10**6)

    links = topo.links()

    for lnk in links:
        if not topo.isSwitch(lnk[0]) or not topo.isSwitch(lnk[1]):
            continue

        lnkInf = topo.linkInfo(lnk[0], lnk[1])

        infc1 = lnkInf["node1"] + "-eth" + str(lnkInf["port1"])
        sp.call(cmd % (infc1), shell = True)

        infc2 = lnkInf["node2"] + "-eth" + str(lnkInf["port2"])
        sp.call(cmd % (infc2), shell = True)

    time.sleep(3)
    startFlows(flows, net)
    CLI(net)
    net.stop()

if __name__ == '__main__':
    main()
